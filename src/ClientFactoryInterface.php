<?php

declare(strict_types=1);

namespace PayPo\Order\API;

interface ClientFactoryInterface
{
    /**
     * @return ClientInterface
     */
    public function create(): ClientInterface;
}