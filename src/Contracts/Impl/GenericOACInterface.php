<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Impl;


use PayPo\Order\API\Http\Response\PaginationValueObject;

interface GenericOACInterface
{
    public const HTTP_GET = 'GET';
    public const HTTP_POST = 'POST';
    public const HTTP_PUT = 'PUT';
    public const HTTP_PATCH = 'PATCH';
    public const HTTP_DELETE = 'DELETE';

    public const IMPL_URI = 'https://order-api.paypo.pl/';
    public const IMPL_TOKEN_ENDPOINT = '/v3/oauth/tokens';
    public const IMPL_GRANT_TYPE = 'client_credentials';

    public const GET_COLLECTION_VALUE_OBJECT = true;
}