<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Impl;


use PayPo\Order\API\Contracts\Entity\EntityInterface;
use PayPo\Order\API\Contracts\Models\FilterModelInterface;
use PayPo\Order\API\Contracts\Models\PaginationModelInterface;
use PayPo\Order\API\Contracts\ValueObjects\ValueObjectInterface;
use PayPo\Order\API\Http\Response\MerchantValueObject;
use PayPo\Order\API\Http\Response\MerchantValueObjectCollection;
use PayPo\Order\API\Request\Payload\Merchants\MerchantPayload;
use PayPo\Order\API\ValueObjects\Merchants\CreatedMerchantValueObject;

interface MerchantsOACInterface extends GenericOACInterface
{
    public const HTTP_REQUEST_ENDPOINT = '/v3/merchants';
}