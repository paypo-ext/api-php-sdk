<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Templates;


use JMS\Serializer\SerializerInterface;

interface ValueObjectDataConverterInterface
{
    /**
     * @param SerializerInterface $serializer
     * @return ValueObjectDataConverterInterface
     */
    public static function getInstance(SerializerInterface $serializer): ValueObjectDataConverterInterface;

    /**
     * @param $data
     * @return array
     */
    public static function convert($data): array;
}