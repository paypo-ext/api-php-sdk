<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Factory;


use Psr\Log\LoggerInterface;

interface LoggerFactoryInterface extends FactoryInterface
{
    public const ORDER_API_SDK_LOGGER = 'order-api-sdk';

    public const INFO_LOG_FILE = self::ORDER_API_SDK_LOGGER . '-info.log';
    public const NOTICE_LOG_FILE = self::ORDER_API_SDK_LOGGER . '-notice.log';
    public const ERROR_LOG_FILE = self::ORDER_API_SDK_LOGGER . '-error.log';

    /**
     * @return LoggerInterface
     */
    public function create(): LoggerInterface;
}