<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Factory;


use PayPo\Order\API\Contracts\Impl\GenericOACInterface;

interface OACImplFactoryInterface
{
    /**
     * @param string $cacheDir
     * @param bool $debug
     * @return GenericOACInterface
     */
    public function create(string $cacheDir, bool $debug = false): GenericOACInterface;
}