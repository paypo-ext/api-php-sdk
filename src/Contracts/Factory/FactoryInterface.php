<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Factory;


interface FactoryInterface
{
    /**
     * @return mixed
     */
    public function create();
}