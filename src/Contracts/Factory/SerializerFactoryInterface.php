<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Factory;


use JMS\Serializer\SerializerInterface;

interface SerializerFactoryInterface extends FactoryInterface
{
    /**
     * @return SerializerInterface
     */
    public function create(): SerializerInterface;
}