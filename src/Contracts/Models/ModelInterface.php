<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Models;


use PayPo\Order\API\Contracts\Entity\EntityInterface;
use PayPo\Order\API\Contracts\Payloads\PayloadInterface;
use PayPo\Order\API\Exceptions\ModelException;
use ReflectionException;

interface ModelInterface
{

    /**
     * @param EntityInterface $entity
     * @return ModelInterface
     */
    public function setEntity(EntityInterface &$entity): ModelInterface;

    /**
     * @return EntityInterface
     */
    public function getEntity(): EntityInterface;

    /**
     * @param PayloadInterface $payload
     * @throws ModelException
     */
    public function fillFromPayload(PayloadInterface $payload);

    /**
     * @param PayloadInterface $payload
     * @throws ModelException
     * @throws ReflectionException
     */
    public function updateFromPayload(PayloadInterface $payload);

    /**
     * @return array
     * @throws ModelException
     * @throws ReflectionException
     */
    public function getEntityParams(): array;

    /**
     * @return array
     */
    public function toArray(): array;

}