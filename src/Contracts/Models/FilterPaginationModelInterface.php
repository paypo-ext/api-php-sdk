<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Models;


interface FilterPaginationModelInterface
{
    /**
     * @return array
     */
    public function toArray(): array;
}