<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Models;


interface PaginationModelInterface extends FilterPaginationModelInterface
{
    public const URI_PARAM_KEY_NAME = 'pagination';
}