<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Payloads\Merchants;


interface MerchantRolePayloadInterface
{
    public const TYPE_ADMIN = 1;
    public const TYPE_PROGRAMMER = 2;
    public const TYPE_USER = 3;
    public const TYPE_GUEST = 4;

    public const TYPE_ADMIN_NAME = 'Administrator';
    public const TYPE_PROGRAMMER_NAME = 'Programista';
    public const TYPE_USER_NAME = 'Użytkownik';
    public const TYPE_GUEST_NAME = 'Inny';

    public const ROLE_TYPES = [
        self::TYPE_ADMIN_NAME => self::TYPE_ADMIN,
        self::TYPE_PROGRAMMER_NAME => self::TYPE_PROGRAMMER,
        self::TYPE_USER_NAME => self::TYPE_USER,
        self::TYPE_GUEST_NAME => self::TYPE_GUEST
    ];

    public const DEFAULT_ROLE_TYPE = self::TYPE_GUEST;
}