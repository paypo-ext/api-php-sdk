<?php


namespace PayPo\Order\API\Contracts\Payloads\Merchants;


interface MerchantSettingsPayloadInterface
{
    public const RATING_NEUTRAL_NAME = 'NEUTRAL';
    public const RATING_PROMO_NAME = 'PROMO';
    public const RATING_VERY_RISKY_NAME = 'VERY RISKY';
    public const RATING_RISKY_NAME = 'RISKY';
    public const RATING_SAFE_NAME = 'SAFE';

    public const RATING_NEUTRAL = 1;
    public const RATING_PROMO = 2;
    public const RATING_VERY_RISKY = 3;
    public const RATING_RISKY = 4;
    public const RATING_SAFE = 5;

    public const RATING_TYPES = [
        self::RATING_NEUTRAL => self::RATING_NEUTRAL_NAME,
        self::RATING_PROMO => self::RATING_PROMO_NAME,
        self::RATING_VERY_RISKY => self::RATING_VERY_RISKY_NAME,
        self::RATING_RISKY => self::RATING_RISKY_NAME,
        self::RATING_SAFE => self::RATING_SAFE_NAME
    ];
}