<?php


namespace PayPo\Order\API\Contracts\Payloads\Merchants;


interface MerchantFeaturesPayloadInterface
{
    public const FEATURE_CORRECTION = 'Korekty';
    public const FEATURE_RETURNS = 'Zwroty';
    public const FEATURE_AUTO_CONFIRM = 'AutoConfirm';
    public const FEATURE_STATUS_REAL = 'StatusReal';
    public const FEATURE_NOT_TRUSTED = 'NotTrusted';
    public const FEATURE_SPLITTED_SHIPMENT = 'SplittedShipment';
    public const FEATURE_PAYPO_RATY = 'PaypoRaty';
    public const FEATURE_MTP = 'MtP';
    public const FEATURE_RR = 'RR';
    public const FEATURE_PROVIDER = 'Provider';
    public const FEATURE_ONLY_TRUSTED = 'OnlyTrusted';
    public const FEATURE_OLD_ORDER_ID = 'OldOrderId';
    public const FEATURE_CUSTOM_CRC = 'CustomCRC';
    public const FEATURE_RATING_NORMALIZATION = 'RatingNormalization';
    public const FEATURE_SKIP_BIG_VERIFICATION = 'SkipBIGVerification';
    public const FEATURE_BASKET_EXPIRATION = 'BasketExpiration';
    public const FEATURE_DRAFT_EXPIRATION = 'DraftExpiration';
    public const FEATURE_SHOPER = 'Shoper';
    public const FEATURE_RETURN_POPUP = 'ReturnPopup';
    public const FEATURE_AUTO_DELIVERY = 'AutoDelivery';
    public const FEATURE_EXTRA_COLUMN = 'ExtraColumnInPPMerchant';
    public const FEATURE_BROKER_AGREEMENT = 'BrokerAgreement';
    public const FEATURE_MANUAL_DELIVERY = 'ManualDelivery';
    public const FEATURE_CUSTOM_SEMAPHORES_LIMIT = 'CustomSemaphoresLimit';
    public const FEATURE_CUSTOM_SOFT_SEMAPHORES_LIMIT = 'CustomSoftSemaphoresLimit';
    public const FEATURE_SKIP_CRIF_VERIFICATION = 'SkipCrifVerification';
    public const FEATURE_CUSTOM_RETURN_LINK_LABELS = 'CustomReturnLinkLabels';
    public const FEATURE_BRAND_GROUPING = 'Brand Grouping';
    public const FEATURE_SUBDOMAIN = 'Subdomain';
    public const FEATURE_HIBP_CHECK = 'checkHaveIBeenPwned';
    public const FEATURE_ZENCARD = 'ZenCard';
    public const FEATURE_P3G = 'P3G';

    public const FEATURES = [
        self::FEATURE_CORRECTION,
        self::FEATURE_RETURNS,
        self::FEATURE_AUTO_CONFIRM,
        self::FEATURE_STATUS_REAL,
        self::FEATURE_NOT_TRUSTED,
        self::FEATURE_SPLITTED_SHIPMENT,
        self::FEATURE_PAYPO_RATY,
        self::FEATURE_MTP,
        self::FEATURE_RR,
        self::FEATURE_PROVIDER,
        self::FEATURE_ONLY_TRUSTED,
        self::FEATURE_OLD_ORDER_ID,
        self::FEATURE_CUSTOM_CRC,
        self::FEATURE_RATING_NORMALIZATION,
        self::FEATURE_SKIP_BIG_VERIFICATION,
        self::FEATURE_BASKET_EXPIRATION,
        self::FEATURE_DRAFT_EXPIRATION,
        self::FEATURE_SHOPER,
        self::FEATURE_RETURN_POPUP,
        self::FEATURE_AUTO_DELIVERY,
        self::FEATURE_EXTRA_COLUMN,
        self::FEATURE_BROKER_AGREEMENT,
        self::FEATURE_MANUAL_DELIVERY,
        self::FEATURE_CUSTOM_SEMAPHORES_LIMIT,
        self::FEATURE_CUSTOM_SOFT_SEMAPHORES_LIMIT,
        self::FEATURE_SKIP_CRIF_VERIFICATION,
        self::FEATURE_CUSTOM_RETURN_LINK_LABELS,
        self::FEATURE_BRAND_GROUPING,
        self::FEATURE_SUBDOMAIN,
        self::FEATURE_HIBP_CHECK,
        self::FEATURE_ZENCARD,
        self::FEATURE_P3G
    ];
}