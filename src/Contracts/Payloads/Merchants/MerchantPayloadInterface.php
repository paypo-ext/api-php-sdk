<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Payloads\Merchants;


interface MerchantPayloadInterface
{
    public const TYPE_SP_JAWNA = 'Spółka jawna';
    public const TYPE_SP_JAWNA_VAL = 1;
    public const TYPE_SP_KOMANDYTOWA = 'Spółka komandytowa';
    public const TYPE_SP_KOMANDYTOWA_VAL = 2;
    public const TYPE_PRZEDSIEBIORSTWO_PANSTWOWE = 'Przedsiębiorstow państwowe';
    public const TYPE_PRZEDSIEBIORSTWO_PANSTWOWE_VAL = 3;
    public const TYPE_SP_AKCYJNA = 'Spółka akcyjna';
    public const TYPE_SP_AKCYJNA_VAL = 4;
    public const TYPE_SP_PARTNERSKA = 'Spółka partnerska';
    public const TYPE_SP_PARTNERSKA_VAL = 5;
    public const TYPE_SP_KOMANDYTOWA_AKCYJNA = 'Spółka komandytowo-akcyjna';
    public const TYPE_SP_KOMANDYTOWA_AKCYJNA_VAL = 6;
    public const TYPE_SP_Z_O_O = 'Spółka z ograniczoną odpowiedzialnością';
    public const TYPE_SP_Z_O_O_VAL = 7;
    public const TYPE_JDG = 'Jednoosobowa działalność gospodarcza (JDG)';
    public const TYPE_JDG_VAL = 8;
    public const TYPE_SP_CYWILNA = 'Spółka cywilna';
    public const TYPE_SP_CYWILNA_VAL = 9;

    public const TYPES = [
        self::TYPE_SP_JAWNA => self::TYPE_SP_JAWNA_VAL,
        self::TYPE_SP_KOMANDYTOWA => self::TYPE_SP_KOMANDYTOWA_VAL,
        self::TYPE_PRZEDSIEBIORSTWO_PANSTWOWE => self::TYPE_PRZEDSIEBIORSTWO_PANSTWOWE_VAL,
        self::TYPE_SP_AKCYJNA => self::TYPE_SP_AKCYJNA_VAL,
        self::TYPE_SP_PARTNERSKA => self::TYPE_SP_PARTNERSKA_VAL,
        self::TYPE_SP_KOMANDYTOWA_AKCYJNA => self::TYPE_SP_KOMANDYTOWA_AKCYJNA_VAL,
        self::TYPE_SP_Z_O_O => self::TYPE_SP_Z_O_O_VAL,
        self::TYPE_SP_CYWILNA => self::TYPE_SP_CYWILNA_VAL,
        self::TYPE_JDG => self::TYPE_JDG_VAL
    ];

    public const DEFAULT_TYPE = 0;
}