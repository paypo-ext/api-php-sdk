<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Payloads\Transfers;


interface TransfersSchedulerPayloadInterface
{
    public const DEFAULT_FILE_NAME = 'pp_{$MID}_transfer_{$S_Y}-{$S_M}-{$S_D}_{$E_Y}-{$E_M}-{$E_D}';

    public const DAY_MONDAY = 'pon';
    public const DAY_TUESDAY = 'wt';
    public const DAY_WEDNESDAY = 'sr';
    public const DAY_THURSDAY = 'cz';
    public const DAY_FRIDAY = 'pt';
    public const DAY_SATURDAY = 'so';
    public const DAY_SUNDAY = 'nd';

    public const DAY_MONDAY_VAL = 1;
    public const DAY_TUESDAY_VAL = 2;
    public const DAY_WEDNESDAY_VAL = 3;
    public const DAY_THURSDAY_VAL = 4;
    public const DAY_FRIDAY_VAL = 5;
    public const DAY_SATURDAY_VAL = 6;
    public const DAY_SUNDAY_VAL = 7;

    public const SCHEDULE_DAYS = [
        self::DAY_MONDAY => self::DAY_MONDAY_VAL,
        self::DAY_TUESDAY => self::DAY_TUESDAY_VAL,
        self::DAY_WEDNESDAY => self::DAY_WEDNESDAY_VAL,
        self::DAY_THURSDAY => self::DAY_THURSDAY_VAL,
        self::DAY_FRIDAY => self::DAY_FRIDAY_VAL,
        self::DAY_SATURDAY => self::DAY_SATURDAY_VAL,
        self::DAY_SUNDAY => self::DAY_SUNDAY_VAL
    ];
}