<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Payloads;


interface FileExportPayloadInterface
{
    public const TYPE_CSV = 1;
    public const TYPE_CSV_NAME = 'csv';
    public const TYPE_XML = 2;
    public const TYPE_XML_NAME = 'xml';
    public const FILE_TYPES = [
        self::TYPE_CSV => self::TYPE_CSV_NAME,
        self::TYPE_XML => self::TYPE_XML_NAME
    ];

    public const DEFAULT_EXPORT_FORMAT_TYPE = self::TYPE_CSV_NAME;
}