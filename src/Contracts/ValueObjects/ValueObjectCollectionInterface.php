<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\ValueObjects;


interface ValueObjectCollectionInterface extends ValueObjectInterface
{
    /**
     * @return DeveloperValueObject[]
     */
    public function getItems(): iterable;

    /**
     * @return int
     */
    public function count(): int;

    /**
     * @return int
     */
    public function getTotalCount(): int;
}