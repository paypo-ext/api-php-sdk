<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\ValueObjects;


use PayPo\Order\API\Contracts\Templates\ValueObjectDataConverterInterface;

interface ValueObjectInterface
{
    /**
     * @param null $data
     * @param ValueObjectDataConverterInterface $converter
     * @return array
     */
    public function convertData($data = null, ValueObjectDataConverterInterface $converter): array;
}