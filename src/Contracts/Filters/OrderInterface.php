<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Filters;


interface OrderInterface
{
    public const ASC = 'ASC';
    public const DESC = 'DESC';

    public const ORDER_DIRECTIONS = [
        self::ASC,
        self::DESC
    ];

    public const DEFAULT_ORDER_BY = 'id';
}