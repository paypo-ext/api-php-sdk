<?php

declare(strict_types=1);

namespace PayPo\Order\API\Contracts\Filters;


interface PaginationInterface
{
    public const DEFAULT_LIMIT = '1024';
    public const DEFAULT_PAGE_COUNT = '1';
}