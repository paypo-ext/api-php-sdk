<?php

declare(strict_types=1);

namespace PayPo\Order\API;

use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function array_merge_recursive;

class Client implements ClientInterface
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var string|null
     */
    private $login;

    /**
     * @var string|null
     */
    private $password;

    /**
     * @var array
     */
    private $defaultOptions = [];

    /**
     * @var null
     */
    private $accessToken = null;

    /**
     * Client constructor.
     *
     * @param HttpClientInterface $httpClient
     * @param string|null         $login
     * @param string|null         $password
     */
    public function __construct(HttpClientInterface $httpClient, $login = null, $password = null)
    {
        $this->httpClient = $httpClient;
        $this->login      = $login;
        $this->password   = $password;
    }

    /**
     * @inheritDoc
     */
    public function setDefaultOptions(array $options): ClientInterface
    {
        $this->defaultOptions = $options;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function request(string $method, string $urn, array $options = []): string
    {
        if (is_null($this->accessToken)) {
            $this->retrieveAccessToken($this->login, $this->password);
        }

        $options = array_merge_recursive(
            $this->defaultOptions,
            $options,
            [
                'auth_bearer' => $this->accessToken,
            ]
        );

        try {
            $response = $this->httpClient->request($method, $urn, $options);

            return $response->getContent(true);
        }
        catch (ClientExceptionInterface $e) {
            if ($e->getCode() === 401) {
                $this->retrieveAccessToken($this->login, $this->password);

                $response = $this->httpClient->request($method, $urn, $options);

                return $response->getContent(true);
            }

            throw $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function retrieveAccessToken(string $login, string $password): string
    {
        $response = $this->httpClient->request(
            'POST',
            'oauth/tokens',
            array_merge_recursive(
                $this->defaultOptions,
                [
                    'body'       => [
                        'grant_type' => 'client_credentials',
                    ],
                    'auth_basic' => [
                        $login,
                        $password,
                    ],
                ]
            )
        );

        $responseArray = $response->toArray();
        if (!array_key_exists('access_token', $responseArray)) {
            throw new ServerException($response);
        }

        $this->accessToken = $responseArray['access_token'];

        return $this->accessToken;
    }
}