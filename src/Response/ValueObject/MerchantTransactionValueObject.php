<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;
use DateTime;

class MerchantTransactionValueObject
{
    const NEW = 'NEW';

    const PENDING = 'PENDING';

    const CANCELLED = 'CANCELLED';

    const REJECTED = 'REJECTED';

    const ACCEPTED = 'ACCEPTED';

    const COMPLETED = 'COMPLETED';

    const PAID = 'PAID';

    const CONFIRMED = 'CONFIRMED';

    const TRANSACTION_STATUSES = [
        self::NEW,
        self::PENDING,
        self::CANCELLED,
        self::REJECTED,
        self::ACCEPTED,
        self::COMPLETED,
    ];

    const SETTLEMENT_STATUSES = [
        self::NEW,
        self::CONFIRMED,
        self::PAID,
    ];

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Uuid()
     */
    private $merchantId;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     */
    private $referenceId;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Uuid()
     */
    private $transactionId;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Choice(choices=MerchantTransactionValueObject::TRANSACTION_STATUSES, message="Choose a valid transaction status.")
     */
    private $transactionStatus;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     *
     * @Assert\Positive()
     */
    private $amount;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Choice(choices=MerchantTransactionValueObject::SETTLEMENT_STATUSES, message="Choose a valid settlement status.")
     */
    private $settlementStatus;


    /**
     * @var DateTimeInterface
     *
     * @Serializer\Type("DateTime")
     *
     */
    private $lastUpdate;

    /**
     * MerchantTransactionValueObject constructor.
     *
     * @param string            $merchantId
     * @param string            $referenceId
     * @param string            $transactionId
     * @param string            $transactionStatus
     * @param int               $amount
     * @param string            $settlementStatus
     * @param DateTimeInterface $lastUpdate
     */
    public function __construct(
        string $merchantId,
        string $referenceId,
        string $transactionId,
        string $transactionStatus,
        int $amount,
        string $settlementStatus,
        DateTimeInterface $lastUpdate
    ) {
        $this->merchantId        = $merchantId;
        $this->referenceId       = $referenceId;
        $this->transactionId     = $transactionId;
        $this->transactionStatus = $transactionStatus;
        $this->amount            = $amount;
        $this->settlementStatus  = $settlementStatus;
        $this->lastUpdate        = $lastUpdate;
    }

    /**
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->merchantId;
    }

    /**
     * @return string
     */
    public function getReferenceId(): string
    {
        return $this->referenceId;
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    /**
     * @return string
     */
    public function getTransactionStatus(): string
    {
        return $this->transactionStatus;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getSettlementStatus(): string
    {
        return $this->settlementStatus;
    }

    /**
     * @return DateTimeInterface
     */
    public function getLastUpdate(): DateTimeInterface
    {
        return $this->lastUpdate;
    }
}