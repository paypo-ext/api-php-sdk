<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterTransactionValueObject
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Uuid()
     */
    private $transactionId;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Url()
     */
    private $redirectUrl;

    /**
     * RegisterTransactionValueObject constructor.
     *
     * @param string $transactionId
     * @param string $redirectUrl
     */
    public function __construct(string $transactionId, string $redirectUrl)
    {
        $this->transactionId = $transactionId;
        $this->redirectUrl   = $redirectUrl;
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }
}