<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject;


use JMS\Serializer\Annotation as Serializer;

class StatusValueObject
{

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     */
    private $code;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $message;

    /**
     * StatusValueObject constructor.
     *
     * @param int $code
     * @param string $message
     */
    public function __construct(int $code, string $message)
    {
        $this->code    = $code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}