<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject;


use JMS\Serializer\Annotation as Serializer;

class OAuthClientValueObject
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $secret;

    /**
     * @var string[]
     *
     * @Serializer\Type("array<string>")
     */
    private $scopes;

    /**
     * @var string[]
     *
     * @Serializer\Type("array<string>")
     */
    private $grantTypes;

    /**
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct($clientId, $clientSecret, array $scopes, array $grantTypes)
    {
        $this->id         = $clientId;
        $this->secret     = $clientSecret;
        $this->scopes     = $scopes;
        $this->grantTypes = $grantTypes;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @return string[]
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @return string[]
     */
    public function getGrantTypes()
    {
        return $this->grantTypes;
    }
}
