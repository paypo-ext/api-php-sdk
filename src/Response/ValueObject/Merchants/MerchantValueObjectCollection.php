<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject\Merchants;

use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Response\ValueObject\AbstractValueObjectCollection;

class MerchantValueObjectCollection extends AbstractValueObjectCollection
{
    /**
     * @var MerchantValueObject[]
     *
     * @Serializer\Type("array<PayPo\Order\API\ValueObject\Response\Merchants\MerchantValueObject>")
     */
    protected $items;
}