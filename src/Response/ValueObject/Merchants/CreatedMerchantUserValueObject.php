<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject\Merchants;


use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Response\ValueObject\AbstractValueObject;

class CreatedMerchantUserValueObject extends AbstractValueObject
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $companyName;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $password;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $username;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $email;


    /**
     * @param string $companyName
     * @param string $password
     * @param string $username
     * @param string $email
     */
    public function __construct(string $companyName, string $password, string $username, string $email)
    {
        $this->companyName = $companyName;
        $this->password = $password;
        $this->username = $username;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

}
