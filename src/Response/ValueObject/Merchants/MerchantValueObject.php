<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject\Merchants;

use JMS\Serializer\Annotation as Serializer;
use DateTime;
use DateTimeInterface;
use PayPo\Order\API\Contracts\ValueObjects\ValueObjectInterface;
use PayPo\Order\API\Response\ValueObject\AbstractValueObject;

class MerchantValueObject extends AbstractValueObject implements ValueObjectInterface
{
    /**
     * @var int
     *
     * @Serializer\Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $type;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $value;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @var DateTimeInterface
     *
     * @Serializer\Type("DateTime")
     */
    private $created;

    /**
     * DeveloperValueObject constructor.
     * @param int $id
     * @param string $type
     * @param string $value
     * @param string $name
     * @param DateTimeInterface $created
     */
    public function __construct(int $id, string $type, string $value, string $name, DateTimeInterface $created)
    {
        $this->id = $id;
        $this->type = $type;
        $this->value = $value;
        $this->name = $name;
        $this->created = $created;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }
}