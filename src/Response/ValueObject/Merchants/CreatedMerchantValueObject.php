<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject\Merchants;


use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Contracts\Templates\ValueObjectDataConverterInterface;
use PayPo\Order\API\Response\ValueObject\AbstractValueObject;
use PayPo\Order\API\Response\ValueObject\OAuthClientValueObject;


class CreatedMerchantValueObject extends AbstractValueObject
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $id;

    /**
     * @var OAuthClientValueObject
     *
     * @Serializer\Type(OAuthClientValueObject::class)
     */
    private $oauthClient;

    /**
     * @var CreatedMerchantUserValueObject
     *
     * @Serializer\Type(CreatedMerchantUserValueObject::class)
     */
    private $createdMerchantUser;


    /**
     * @param string $id
     * @param CreatedMerchantUserValueObject $createdMerchantUser
     */
    public function __construct(
        string $id,
        CreatedMerchantUserValueObject $createdMerchantUser
    )
    {
        $this->id = $id;
        $this->createdMerchantUser = $createdMerchantUser;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CreatedMerchantUserValueObject
     */
    public function getCreatedMerchantUser(): CreatedMerchantUserValueObject
    {
        return $this->createdMerchantUser;
    }
}
