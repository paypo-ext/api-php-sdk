<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject;


use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Contracts\ValueObjects\ValueObjectCollectionInterface;
use PayPo\Order\API\Contracts\ValueObjects\ValueObjectInterface;

/**
 * Class AbstractValueObjectCollection
 * @package PayPo\Order\API\Response\ValueObject
 * @Serializer\ExclusionPolicy ("none")
 */
class AbstractValueObjectCollection extends AbstractValueObject implements ValueObjectCollectionInterface
{
    /**
     * @var ValueObjectInterface[]
     *
     * @Serializer\Type("array<PayPo\Order\API\Contracts\ValueObjects\ValueObjectInterface>")
     */
    protected $items;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("totalCount")
     */
    protected $totalCount;


    /**
     * DeveloperValueObjectCollection constructor.
     * @param ValueObjectInterface[] $items
     * @param int $total
     */
    public function __construct(iterable $items = [], int $total)
    {
        $this->items = $items;
        $this->totalCount = $total;
    }

    /**
     * @return ValueObjectInterface[]
     */
    public function getItems(): iterable
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items ?? []);
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

}