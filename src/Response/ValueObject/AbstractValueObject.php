<?php

declare(strict_types=1);

namespace PayPo\Order\API\Response\ValueObject;


use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Contracts\Templates\ValueObjectDataConverterInterface;
use PayPo\Order\API\Contracts\ValueObjects\ValueObjectInterface;

/**
 * Class AbstractValueObject
 * @package PayPo\Order\API\Response\ValueObject
 * @Serializer\ExclusionPolicy ("none")
 */
class AbstractValueObject implements ValueObjectInterface
{
    /**
     * @param null $data
     * @param ValueObjectDataConverterInterface $converter
     * @return array
     */
    public function convertData($data = null, ValueObjectDataConverterInterface $converter): array
    {
        return $converter::convert($data ?? $this);
    }
}