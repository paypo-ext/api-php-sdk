<?php

declare(strict_types=1);

namespace PayPo\Order\API\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class NotExistsInDatabase extends Constraint
{
    /**
     * @var string
     */
    public $dbFieldName;

    /**
     * @var string
     */
    public $entityRepositoryName;

    /**
     * @var string
     */
    public $valuePath = null;

    /**
     * @var string
     */
    public $message = 'Given value {{ value }} exists in the database.';
}