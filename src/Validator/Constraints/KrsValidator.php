<?php

declare(strict_types=1);

namespace PayPo\Order\API\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use PayPo\Order\API\Contracts\Payloads\PayloadInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class KrsValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {

        if (!$constraint instanceof Krs) {
            throw new UnexpectedTypeException($constraint, NotExistsInDatabase::class);
        }

        if ($constraint->dependsOnValuePath !== null) {
            $restrictionValue = $this->getValueFromPath($value, $constraint->dependsOnValuePath);
            if (in_array($restrictionValue, $constraint->disableForValues)) {
                return;
            }
        }

        if (null === $value || '' === $value) {
            $this->addViolationMsg(
                $constraint->message . ' The KRS number cannot be empty.',
                $value
            );
            return;
        }

        if (!preg_match('/^(?<krs>(KRS)?[\\s]?[\\d]{4}([-\\s]?[\\d]{3}){2})$/', $value, $matches)) {
            $this->addViolationMsg(
                $constraint->message . " Valid format 'KRS 0000-000-000'.",
                $value
            );
        }
    }

    /**
     * @param string $msg
     * @param $value
     */
    private
    function addViolationMsg(string $msg, $value)
    {
        $this->context->buildViolation($msg)
            ->setParameter('{{ value }}', (string)$value)
            ->atPath($constraint->valuePath ?? '')
            ->addViolation();
    }

    /**
     * @param $value
     * @param $path
     * @return PayloadInterface
     */
    private function getValueFromPath($value, $path)
    {
        $outcome = null;
        $path = explode('.', $path);
        $targetFieldGetMethodName = $this->prepareParamGetMethodName(array_pop($path));

        if(!empty($path)) {
            foreach ((array)$path as $name) {
                if (is_array($value)) {
                    $value = $value[$name];
                } else {
                    $getMethod = $this->prepareParamGetMethodName($name);
                    $value = $value->{$getMethod}();
                }
            }
            $outcome = $value->{$targetFieldGetMethodName}();
        }else{
            $outcome = $this->context->getRoot()->{$targetFieldGetMethodName}();
        }

        return $outcome;
    }

    /**
     * @param $field
     * @return string
     */
    private
    function prepareParamGetMethodName($field): string
    {
        return 'get' . ucfirst(strtolower($field));
    }
}