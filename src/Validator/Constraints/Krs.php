<?php
declare(strict_types=1);

namespace PayPo\Order\API\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Krs extends Constraint
{
    /**
     * @var string
     */
    public $dependsOnValuePath = null;

    /**
     * @var array
     */
    public $disableForValues = [];

    /**
     * @var string
     */
    public $message = 'Given KRS ({{ value }}) is not valid.';
}