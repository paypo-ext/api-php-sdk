<?php

declare(strict_types=1);

namespace PayPo\Order\API\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use PayPo\Order\API\Contracts\Payloads\PayloadInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NotExistsInDatabaseValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $repositoryNamespace;

    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * NotExistsInDatabaseValidator constructor.
     * @param ManagerRegistry $registry
     * @param string $repositoryNamespace
     */
    public function __construct(ManagerRegistry $registry, string $repositoryNamespace)
    {
        $this->registry = $registry;
        $this->repositoryNamespace = $repositoryNamespace;
    }


    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof NotExistsInDatabase) {
            throw new UnexpectedTypeException($constraint, NotExistsInDatabase::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $entityRepositoryClassName = $this->repositoryNamespace . '\\' . $constraint->entityRepositoryName;

        $repo = new $entityRepositoryClassName($this->registry);

        if ($constraint->valuePath !== null) {
            $value = $this->getValueFromPath($value, $constraint->valuePath);
        }

        if ($repo->findOneBy([$constraint->dbFieldName => $value])) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->atPath($constraint->valuePath ?? '')
                ->addViolation();
        }

    }

    /**
     * @param $value
     * @param $path
     * @return PayloadInterface
     */
    private function getValueFromPath($value, $path)
    {
        $path = explode('.', $path);
        $targetFieldGetMethodName = $this->prepareParamGetMethodName(array_pop($path));

        foreach ((array)$path as $name) {
            if (is_array($value)) {
                $value = $value[$name];
            } else {
                $getMethod = $this->prepareParamGetMethodName($name);
                $value = $value->{$getMethod}();
            }
        }

        return $value->{$targetFieldGetMethodName}();
    }

    /**
     * @param $field
     * @return string
     */
    private function prepareParamGetMethodName($field): string
    {
        return 'get' . ucfirst(strtolower($field));
    }
}