<?php

declare(strict_types=1);

namespace PayPo\Order\API;

use Symfony\Component\HttpClient\HttpClient;

class ClientFactory implements ClientFactoryInterface
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var string|null
     */
    private $login;

    /**
     * @var string|null
     */
    private $password;

    /**
     * ClientFactory constructor.
     *
     * @param string      $uri
     * @param string|null $login
     * @param string|null $password
     */
    public function __construct(string $uri, $login = null, $password = null)
    {
        $this->uri      = $uri;
        $this->login    = $login;
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    public function create(): ClientInterface
    {
        return new Client(
            HttpClient::createForBaseUri($this->uri), $this->login, $this->password
        );
    }
}