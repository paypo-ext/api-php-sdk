<?php

declare(strict_types=1);

namespace PayPo\Order\API;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

interface ClientInterface
{

    /**
     * @param array $options
     *
     * @return ClientInterface
     * @see HttpClientInterface::OPTIONS_DEFAULTS
     */
    public function setDefaultOptions(array $options): self;

    /**
     * @param string $login
     * @param string $password
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return string
     */
    public function retrieveAccessToken(string $login, string $password): string;

    /**
     * @param string $method
     * @param string $urn
     * @param array  $options
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return string
     */
    public function request(string $method, string $urn, array $options = []): string;
}