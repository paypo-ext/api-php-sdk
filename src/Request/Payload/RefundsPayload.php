<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;

use Symfony\Component\Validator\Constraints as Assert;

class RefundsPayload
{
    /**
     * @Assert\Positive()
     *
     * @var int
     */
    private $amount;

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     *
     * @return RefundsPayload
     */
    public function setAmount(int $amount): RefundsPayload
    {
        $this->amount = $amount;

        return $this;
    }
}