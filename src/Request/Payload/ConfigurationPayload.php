<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;

use Symfony\Component\Validator\Constraints as Assert;

class ConfigurationPayload
{
    /**
     * @Assert\NotNull()
     * @Assert\Url()
     *
     * @var string
     */
    private $returnUrl;

    /**
     * @Assert\NotNull()
     * @Assert\Url()
     *
     * @var string
     */
    private $notifyUrl;

    /**
     * @Assert\Url()
     *
     * @var string|null
     */
    private $cancelUrl = null;

    /**
     * @return string
     */
    public function getReturnUrl(): string
    {
        return $this->returnUrl;
    }

    /**
     * @param string $returnUrl
     *
     * @return ConfigurationPayload
     */
    public function setReturnUrl(string $returnUrl): ConfigurationPayload
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string $notifyUrl
     *
     * @return ConfigurationPayload
     */
    public function setNotifyUrl(string $notifyUrl): ConfigurationPayload
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCancelUrl(): ?string
    {
        return $this->cancelUrl;
    }

    /**
     * @param string|null $cancelUrl
     *
     * @return ConfigurationPayload
     */
    public function setCancelUrl(?string $cancelUrl): ConfigurationPayload
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }
}