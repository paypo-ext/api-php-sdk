<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Contracts\Payloads\Merchants\MerchantRolePayloadInterface;


class PersonPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     */
    private $surname;

    /**
     * @var EmailPayload
     *
     * @Serializer\Type(EmailPayload::class)
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type(EmailPayload::class)
     * @Assert\Valid()
     */
    private $email;

    /**
     * @var PhonePayload
     *
     * @Serializer\Type(PhonePayload::class)
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type(PhonePayload::class)
     * @Assert\Valid()
     */
    private $phone;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Choice(choices=MerchantRolePayloadInterface::ROLE_TYPES, message="Choose a valid role type. Allowed: {{ choices }}")
     */
    private $roleId = '';

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     *
     * @Assert\Type("bool")
     */
    private $active = false;


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PersonPayload
     */
    public function setName(string $name): PersonPayload
    {
        //TODO String convert by u()
        $value      = mb_strtoupper(trim($name), 'UTF-8');
        $this->name = $value ? (string) $value : $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return PersonPayload
     */
    public function setSurname(string $surname): PersonPayload
    {
        //TODO String convert by u()
        $value         = mb_strtoupper(trim($surname), 'UTF-8');
        $this->surname = $value ? (string) $value : $surname;

        return $this;
    }

    /**
     * @return EmailPayload
     */
    public function getEmail(): EmailPayload
    {
        return $this->email;
    }

    /**
     * @param EmailPayload $email
     * @return PersonPayload
     */
    public function setEmail(EmailPayload $email): PersonPayload
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return PhonePayload
     */
    public function getPhone(): PhonePayload
    {
        return $this->phone;
    }

    /**
     * @param PhonePayload $phone
     *
     * @return PersonPayload
     */
    public function setPhone(PhonePayload $phone): PersonPayload
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     * @return PersonPayload
     */
    public function setRoleId(int $roleId): PersonPayload
    {
        $this->roleId = $roleId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active ?? true;
    }

    /**
     * @param bool $active
     * @return PersonPayload
     */
    public function setActive(bool $active): PersonPayload
    {
        $this->active = $active;
        return $this;
    }
}