<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;

use Symfony\Component\Validator\Constraints as Assert;

class CustomerPayload
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $name;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $surname;

    /**
     * @Assert\NotNull()
     * @Assert\Email()
     *
     * @var string
     */
    private $email;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CustomerPayload
     */
    public function setName(string $name): CustomerPayload
    {
        $value      = mb_strtoupper(trim($name), 'UTF-8');
        $this->name = $value ? (string) $value : $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return CustomerPayload
     */
    public function setSurname(string $surname): CustomerPayload
    {
        $value         = mb_strtoupper(trim($surname), 'UTF-8');
        $this->surname = $value ? (string) $value : $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return CustomerPayload
     */
    public function setEmail(string $email): CustomerPayload
    {
        $this->email = strtolower(trim($email));

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return CustomerPayload
     */
    public function setPhone(?string $phone): CustomerPayload
    {
        $this->phone = $phone;

        return $this;
    }
}
