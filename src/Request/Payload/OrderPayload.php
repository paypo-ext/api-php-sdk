<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;

use Symfony\Component\Validator\Constraints as Assert;

class OrderPayload
{
    const SHIPMENT_TYPES = [
        self::COURIER,
        self::ACCESS_POINT,
        self::PARCEL_LOCKER,
        self::PARCEL_LOCKER_RUCH,
        self::CLICK_AND_COLLECT,
    ];

    const COURIER = 0;

    const ACCESS_POINT = 1;

    const PARCEL_LOCKER = 2;

    const PARCEL_LOCKER_RUCH = 3;

    const CLICK_AND_COLLECT = 4;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $referenceId;

    /**
     * @var string|null
     */
    private $providerId = null;

    /**
     * @var string|null
     */
    private $description = null;

    /**
     * @var string[]|null
     */
    private $additionalInfo = null;

    /**
     * @Assert\NotNull()
     * @Assert\Positive(message="Order amount should be positive")
     *
     * @var int
     */
    private $amount;

    /**
     * @Assert\Valid()
     * @Assert\NotNull()
     *
     * @var AddressPayload
     */
    private $billingAddress;

    /**
     * @Assert\Valid()
     * @Assert\NotNull()
     *
     * @var AddressPayload
     */
    private $shippingAddress;

    /**
     * @Assert\Choice(choices=OrderPayload::SHIPMENT_TYPES, message="Choose a valid shipment type.")
     *
     * @var int|null
     */
    private $shipment = null;

    /**
     * @return string
     */
    public function getReferenceId(): string
    {
        return $this->referenceId;
    }

    /**
     * @param string $referenceId
     *
     * @return OrderPayload
     */
    public function setReferenceId(string $referenceId): OrderPayload
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProviderId(): ?string
    {
        return $this->providerId;
    }

    /**
     * @param string|null $providerId
     *
     * @return OrderPayload
     */
    public function setProviderId(?string $providerId): OrderPayload
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return OrderPayload
     */
    public function setDescription(?string $description): OrderPayload
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getAdditionalInfo(): ?array
    {
        return $this->additionalInfo;
    }

    /**
     * @param array|null $additionalInfo
     *
     * @return OrderPayload
     */
    public function setAdditionalInfo(?array $additionalInfo): OrderPayload
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     *
     * @return OrderPayload
     */
    public function setAmount(int $amount): OrderPayload
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return AddressPayload
     */
    public function getBillingAddress(): AddressPayload
    {
        return $this->billingAddress;
    }

    /**
     * @param AddressPayload $billingAddress
     *
     * @return OrderPayload
     */
    public function setBillingAddress(AddressPayload $billingAddress): OrderPayload
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * @return AddressPayload
     */
    public function getShippingAddress(): AddressPayload
    {
        return $this->shippingAddress;
    }

    /**
     * @param AddressPayload $shippingAddress
     *
     * @return OrderPayload
     */
    public function setShippingAddress(AddressPayload $shippingAddress): OrderPayload
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getShipment(): ?int
    {
        return $this->shipment;
    }

    /**
     * @param int|null $shipment
     *
     * @return OrderPayload
     */
    public function setShipment(?int $shipment): OrderPayload
    {
        $this->shipment = $shipment;

        return $this;
    }
}
