<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload\Pagination;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Contracts\Filters\PaginationInterface;

final class Pagination implements PaginationInterface
{

    /**
     * @var string
     *
     * @Assert\Range(min=PaginationInterface::DEFAULT_PAGE_COUNT)
     *
     * @Serializer\Type("string")
     */
    private $page = self::DEFAULT_PAGE_COUNT;

    /**
     * @var string
     *
     * @Assert\Range(min=PaginationInterface::DEFAULT_PAGE_COUNT)
     *
     * @Serializer\Type("string")
     */
    private $limit = self::DEFAULT_LIMIT;

    /**
     * @var Order|null
     *
     * @Assert\Type(Order::class)
     *
     * @Serializer\Type(Order::class)
     */
    private $order;

    /**
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param string $page
     *
     * @return self
     */
    public function setPage($page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return self
     */
    public function setLimit($limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return Order|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order|null $order
     *
     * @return Pagination
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOrderSet()
    {
        return isset($this->order);
    }
}
