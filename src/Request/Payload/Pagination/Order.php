<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload\Pagination;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use PayPo\Order\API\Contracts\Filters\OrderInterface;

final class Order implements OrderInterface
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Type("string")
     */
    private $by = self::DEFAULT_ORDER_BY;

    /**
     * @var string
     *
     * @Assert\Choice(choices=OrderInterface::ORDER_DIRECTIONS)
     *
     * @Serializer\Type("string")
     */
    private $direction = self::DESC;


    /**
     * @return string
     */
    public function getBy()
    {
        return $this->by;
    }

    /**
     * @param string $by
     *
     * @return Order
     */
    public function setBy($by)
    {
        $this->by = $by;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBySet()
    {
        return isset($this->by);
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     *
     * @return Order
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDirectionSet()
    {
        return isset($this->direction);
    }
}
