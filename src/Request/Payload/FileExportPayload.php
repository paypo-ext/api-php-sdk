<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Contracts\Payloads\FileExportPayloadInterface;

class FileExportPayload extends AbstractPayload implements FileExportPayloadInterface
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Choice(choices=FileExportPayloadInterface::FILE_TYPES, message="Choose a valid file type. Allowed: {{ choices }}")
     * @Assert\Type("string")
     */
    private $type = self::DEFAULT_EXPORT_FORMAT_TYPE;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return FileExportPayload
     */
    public function setType(string $type): FileExportPayload
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FileExportPayload
     */
    public function setName(string $name): FileExportPayload
    {
        $this->name = $name;
        return $this;
    }
}