<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;

use Symfony\Component\Validator\Constraints as Assert;

class RegisterTransactionPayload
{

    /**
     * @Assert\Uuid()
     */
    private $id = null;


    /**
     * @Assert\Uuid()
     *
     * @var string|null
     */
    private $shopId = null;

    /**
     * @Assert\Valid()
     * @Assert\NotNull()
     *
     * @var OrderPayload
     */
    private $order;

    /**
     * @Assert\Valid()
     * @Assert\NotNull()
     *
     * @var CustomerPayload
     */
    private $customer;

    /**
     * @Assert\Valid()
     * @Assert\NotNull()
     *
     * @var ConfigurationPayload
     */
    private $configuration;

    /**
     * @return string|null
     */
    public function getShopId(): ?string
    {
        return $this->shopId;
    }

    /**
     * @param string|null $shopId
     *
     * @return RegisterTransactionPayload
     */
    public function setShopId(?string $shopId): RegisterTransactionPayload
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * @return OrderPayload
     */
    public function getOrder(): OrderPayload
    {
        return $this->order;
    }

    /**
     * @param OrderPayload $order
     *
     * @return RegisterTransactionPayload
     */
    public function setOrder(OrderPayload $order): RegisterTransactionPayload
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return CustomerPayload
     */
    public function getCustomer(): CustomerPayload
    {
        return $this->customer;
    }

    /**
     * @param CustomerPayload $customer
     *
     * @return RegisterTransactionPayload
     */
    public function setCustomer(CustomerPayload $customer): RegisterTransactionPayload
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return ConfigurationPayload
     */
    public function getConfiguration(): ConfigurationPayload
    {
        return $this->configuration;
    }

    /**
     * @param ConfigurationPayload $configuration
     *
     * @return RegisterTransactionPayload
     */
    public function setConfiguration(ConfigurationPayload $configuration): RegisterTransactionPayload
    {
        $this->configuration = $configuration;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): self
    {
        $this->id = $id;

        return $this;
    }
}
