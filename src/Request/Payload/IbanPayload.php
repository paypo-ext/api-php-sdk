<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Request\Payload\AbstractPayload;

class IbanPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Iban()
     */
    private $iban;

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     */
    private $active = true;

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     * @return IbanPayload
     */
    public function setIban(string $iban): IbanPayload
    {
        $this->iban = $iban;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return IbanPayload
     */
    public function setActive(bool $active): IbanPayload
    {
        $this->active = $active;
        return $this;
    }
}