<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use function Symfony\Component\String\u;

class AddressPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $street;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     *
     * @Assert\Type("string")
     */
    private $building = null;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     *
     * @Assert\Type("string")
     */
    private $flat = null;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^\d+-\d+$/", message="This value is not a valid postal code.")
     */
    private $zip;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $city;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     *
     * @Assert\Country()
     */
    private $country = null;

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return AddressPayload
     */
    public function setStreet(string $street): AddressPayload
    {
        $this->street = u($street)->upper()->toString();

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBuilding(): ?string
    {
        return $this->building;
    }

    /**
     * @param string|null $building
     *
     * @return AddressPayload
     */
    public function setBuilding(?string $building): AddressPayload
    {
        $this->building = is_null($building) ? $building : strtoupper(trim($building));

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFlat(): ?string
    {
        return $this->flat;
    }

    /**
     * @param string|null $flat
     *
     * @return AddressPayload
     */
    public function setFlat(?string $flat): AddressPayload
    {
        $this->flat = is_null($flat) ? $flat : strtoupper(trim($flat));

        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return AddressPayload
     */
    public function setZip(string $zip): AddressPayload
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return AddressPayload
     */
    public function setCity(string $city): AddressPayload
    {
        $this->city = u($city)->upper()->toString();

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return AddressPayload
     */
    public function setCountry(?string $country): AddressPayload
    {
        $this->country = is_null($country) ? $country : strtoupper(trim($country));

        return $this;
    }
}