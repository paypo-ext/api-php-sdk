<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ApiTokenPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $uri;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $token;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     */
    private $apiService;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     */
    private $version;

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     */
    private $active = false;

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return ApiTokenPayload
     */
    public function setUri(string $uri): ApiTokenPayload
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return ApiTokenPayload
     */
    public function setToken(string $token): ApiTokenPayload
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return int
     */
    public function getApiService(): int
    {
        return $this->apiService;
    }

    /**
     * @param int $apiService
     * @return ApiTokenPayload
     */
    public function setApiService(int $apiService): ApiTokenPayload
    {
        $this->apiService = $apiService;
        return $this;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return ApiTokenPayload
     */
    public function setVersion(string $version): ApiTokenPayload
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return ApiTokenPayload
     */
    public function setActive(bool $active): ApiTokenPayload
    {
        $this->active = $active;
        return $this;
    }
}