<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload\Merchants\Filters;


use PayPo\Order\API\Contracts\Filters\FilterInterface;

final class MerchantFilter implements FilterInterface
{
}
