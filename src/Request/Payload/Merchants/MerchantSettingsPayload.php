<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload\Merchants;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Request\Payload\IbanPayload;
use PayPo\Order\API\Request\Payload\AbstractPayload;
use PayPo\Order\API\Contracts\Payloads\Merchants\MerchantSettingsPayloadInterface;

class MerchantSettingsPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Url
     */
    private $www;

    /**
     * @var IbanPayload
     *
     * @Serializer\Type(IbanPayload::class)
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type(IbanPayload::class)
     * @Assert\Valid()
     */
    private $account;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("integer")
     */
    private $provision;

    /**
     * @var float
     *
     * @Serializer\Type("float")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("float")
     */
    private $commission;

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     *
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $trustedCustomers = false;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("integer")
     */
    private $grace;

    /**
     * @var MerchantFeaturePayload[]
     *
     * @Serializer\Type("array<PayPo\Order\API\Request\Payload\Merchants\MerchantFeaturePayload>")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type("iterable")
     * @Assert\Valid()
     */
    private $features;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Choice(choices=MerchantSettingsPayloadInterface::RATING_TYPES, message="Choose a valid rating type. Allowed: {{ choices }}")
     */
    private $rating = '';

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     *
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $p3g = false;


    /**
     * @return string
     */
    public function getWww(): string
    {
        return $this->www;
    }

    /**
     * @param string $www
     * @return MerchantSettingsPayload
     */
    public function setWww(string $www): MerchantSettingsPayload
    {
        $this->www = $www;
        return $this;
    }

    /**
     * @return IbanPayload
     */
    public function getAccount(): IbanPayload
    {
        return $this->account;
    }

    /**
     * @param IbanPayload $account
     * @return MerchantSettingsPayload
     */
    public function setAccount(IbanPayload $account): MerchantSettingsPayload
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return int
     */
    public function getProvision(): int
    {
        return $this->provision;
    }

    /**
     * @param int $provision
     * @return MerchantSettingsPayload
     */
    public function setProvision(int $provision): MerchantSettingsPayload
    {
        $this->provision = $provision;
        return $this;
    }

    /**
     * @return float
     */
    public function getCommission(): float
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     * @return MerchantSettingsPayload
     */
    public function setCommission(float $commission): MerchantSettingsPayload
    {
        $this->commission = $commission;
        return $this;
    }

    /**
     * @return bool
     */
    public function getTrustedCustomers(): bool
    {
        return $this->trustedCustomers;
    }

    /**
     * @param bool $trustedCustomers
     * @return MerchantSettingsPayload
     */
    public function setTrustedCustomers(bool $trustedCustomers): MerchantSettingsPayload
    {
        $this->trustedCustomers = $trustedCustomers;
        return $this;
    }

    /**
     * @return int
     */
    public function getGrace(): int
    {
        return $this->grace;
    }

    /**
     * @param int $grace
     * @return MerchantSettingsPayload
     */
    public function setGrace(int $grace): MerchantSettingsPayload
    {
        $this->grace = $grace;
        return $this;
    }

    /**
     * @return MerchantFeaturePayload[]
     */
    public function getFeatures(): iterable
    {
        return $this->features;
    }

    /**
     * @param iterable $features
     * @return MerchantSettingsPayload
     */
    public function setFeatures(iterable $features): MerchantSettingsPayload
    {
        $this->features = $features;
        return $this;
    }

    /**
     * @return string
     */
    public function getRating(): string
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     * @return MerchantSettingsPayload
     */
    public function setRating(string $rating): MerchantSettingsPayload
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return bool
     */
    public function getP3g(): bool
    {
        return $this->p3g;
    }

    /**
     * @param bool $p3g
     * @return MerchantSettingsPayload
     */
    public function setP3g(bool $p3g): MerchantSettingsPayload
    {
        $this->p3g = $p3g;
        return $this;
    }
}