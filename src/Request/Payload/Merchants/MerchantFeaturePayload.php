<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload\Merchants;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Request\Payload\AbstractPayload;
use PayPo\Order\API\Contracts\Payloads\Merchants\MerchantFeaturesPayloadInterface;

class MerchantFeaturePayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Choice(choices=MerchantFeaturesPayloadInterface::FEATURES, message="Choose a valid feature type. Allowed: {{ choices }}")
     */
    private $featureKey;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\Type("string")
     */
    private $featureParams;

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     *
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $active = false;

    /**
     * @return string
     */
    public function getFeatureKey(): string
    {
        return $this->featureKey;
    }

    /**
     * @param string $featureKey
     * @return MerchantFeaturePayload
     */
    public function setFeatureKey(string $featureKey): MerchantFeaturePayload
    {
        $this->featureKey = $featureKey;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFeatureParams(): ?string
    {
        return $this->featureParams;
    }

    /**
     * @param string $featureParams
     * @return MerchantFeaturePayload
     */
    public function setFeatureParams(string $featureParams): MerchantFeaturePayload
    {
        $this->featureParams = $featureParams;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return MerchantFeaturePayload
     */
    public function setActive(bool $active): MerchantFeaturePayload
    {
        $this->active = $active;
        return $this;
    }
}