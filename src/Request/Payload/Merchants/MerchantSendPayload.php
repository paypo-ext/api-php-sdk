<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload\Merchants;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Request\Payload\AbstractPayload;
use PayPo\Order\API\Request\Payload\EmailPayload;

class MerchantSendPayload extends AbstractPayload
{
    /**
     * @var EmailPayload[]
     *
     * @Serializer\Type("array<PayPo\Order\API\Request\Payload\EmailPayload>")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Valid()
     */
    private $to = [];

    /**
     * @var EmailPayload[]
     *
     * @Serializer\Type("array<PayPo\Order\API\Request\Payload\EmailPayload>")
     *
     * @Assert\Valid()
     */
    private $dw = [];

    /**
     * @return EmailPayload[]
     */
    public function getTo(): array
    {
        return $this->to;
    }

    /**
     * @param EmailPayload[] $to
     * @return MerchantSendPayload
     */
    public function setTo(array $to): MerchantSendPayload
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return EmailPayload[]
     */
    public function getDw(): array
    {
        return $this->dw;
    }

    /**
     * @param EmailPayload[] $dw
     * @return MerchantSendPayload
     */
    public function setDw(array $dw): MerchantSendPayload
    {
        $this->dw = $dw;
        return $this;
    }
}