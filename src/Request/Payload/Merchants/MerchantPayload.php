<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload\Merchants;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Validator\Constraints as CustomAssert;
use PayPo\Order\API\Request\Payload\AbstractPayload;
use PayPo\Order\API\Request\Payload\PersonPayload;
use PayPo\Order\API\Request\Payload\AddressPayload;
use PayPo\Order\API\Request\Payload\Transfers\TransfersSchedulerPayload;
use PayPo\Order\API\Contracts\Payloads\Merchants\MerchantPayloadInterface;

/**
 * Class MerchantPayload
 * @package PayPo\Order\API\Request\Payload\Merchants
 *
 * @Serializer\ExclusionPolicy
 */
class MerchantPayload extends AbstractPayload implements MerchantPayloadInterface
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $companyName = '';

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $businessName = '';

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=MerchantPayloadInterface::TYPES, message="Choose a valid company type. Allowed: {{ choices }}")
     * @Assert\Type("integer")
     */
    private $type;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Regex(
     *      pattern="/^(?<nip>[\d]{3}([-\s]?[\d]{3}){1}([-\s]?[\d]{2}){2})$/",
     *      message="Given NIP ({{ value }}) is not valid. Valid format '000-000-00-00'."
     * )
     */
    private $nip;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @CustomAssert\Krs(dependsOnValuePath="type", disableForValues={MerchantPayloadInterface::TYPE_JDG_VAL,MerchantPayloadInterface::TYPE_SP_CYWILNA_VAL})
     */
    private $krs;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Date(message="Registration date is not valid")
     */
    private $registrationDate;

    /**
     * @var AddressPayload
     *
     * @Serializer\Type(AddressPayload::class)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(AddressPayload::class)
     * @Assert\Valid()
     */
    private $address;

    /**
     * @var string
     *
     * @Serializer\ReadOnly
     *
     * @Serializer\Exclude
     */
    private $contactAddress;

    /**
     * @var PersonPayload
     *
     * @Serializer\Type(PersonPayload::class)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(PersonPayload::class)
     * @Assert\Valid()
     */
    private $person;

    /**
     * @var MerchantSettingsPayload
     *
     * @Serializer\Type(MerchantSettingsPayload::class)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    private $settings;

    /**
     * @var TransfersSchedulerPayload
     *
     * @Serializer\Type(TransfersSchedulerPayload::class)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    private $schedule;


    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     * @return MerchantPayload
     */
    public function setCompanyName(string $companyName): MerchantPayload
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessName(): string
    {
        return $this->businessName;
    }

    /**
     * @param string $businessName
     * @return MerchantPayload
     */
    public function setBusinessName(string $businessName): MerchantPayload
    {
        $this->businessName = $businessName;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return MerchantPayload
     */
    public function setType(int $type): MerchantPayload
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getNip(): string
    {
        return $this->nip;
    }

    /**
     * @param string $nip
     * @return MerchantPayload
     */
    public function setNip(string $nip): MerchantPayload
    {
        $this->nip = $nip;
        return $this;
    }

    /**
     * @return string
     */
    public function getKrs(): string
    {
        return $this->krs;
    }

    /**
     * @param string $krs
     * @return MerchantPayload
     */
    public function setKrs(string $krs): MerchantPayload
    {
        $this->krs = $krs;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationDate(): string
    {
        return $this->registrationDate;
    }

    /**
     * @param string $registrationDate
     * @return MerchantPayload
     */
    public function setRegistrationDate(string $registrationDate): MerchantPayload
    {
        $this->registrationDate = $registrationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactAddress(): string
    {
        return $this->contactAddress;
    }

    /**
     * @param string $contactAddress
     * @return MerchantPayload
     */
    private function setContactAddress(string $contactAddress): MerchantPayload
    {
        $this->contactAddress = $contactAddress;
        return $this;
    }

    /**
     * @return AddressPayload
     */
    public function getAddress(): AddressPayload
    {
        return $this->address;
    }


    /**
     * @param AddressPayload $address
     * @return MerchantPayload
     */
    public function setAddress(AddressPayload $address): MerchantPayload
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return PersonPayload
     */
    public function getPerson(): PersonPayload
    {
        return $this->person;
    }

    /**
     * @param PersonPayload $person
     * @return MerchantPayload
     */
    public function setPerson(PersonPayload $person): MerchantPayload
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return MerchantSettingsPayload
     */
    public function getSettings(): MerchantSettingsPayload
    {
        return $this->settings;
    }

    /**
     * @param MerchantSettingsPayload $settings
     * @return MerchantPayload
     */
    public function setSettings(MerchantSettingsPayload $settings): MerchantPayload
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * @return TransfersSchedulerPayload
     */
    public function getSchedule(): TransfersSchedulerPayload
    {
        return $this->schedule;
    }

    /**
     * @param TransfersSchedulerPayload $schedule
     * @return MerchantPayload
     */
    public function setSchedule(TransfersSchedulerPayload $schedule): MerchantPayload
    {
        $this->schedule = $schedule;
        return $this;
    }
}