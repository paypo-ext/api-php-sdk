<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload\Transfers;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use PayPo\Order\API\Request\Payload\Merchants\MerchantSendPayload;
use PayPo\Order\API\Request\Payload\FileExportPayload;
use PayPo\Order\API\Request\Payload\AbstractPayload;
use PayPo\Order\API\Contracts\Payloads\Transfers\TransfersSchedulerPayloadInterface;
use PayPo\Order\API\Validator\Constraints as CustomAssert;

class TransfersSchedulerPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @CustomAssert\NotExistsInDatabase(
     *     dbFieldName = "name",
     *     entityRepositoryName = "TransfersSchedulerRepository"
     * )
     */
    private $name;

    /**
     * @var int[]
     *
     * @Serializer\Type("array<integer>")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\All({
     *     @Assert\Choice(choices=TransfersSchedulerPayloadInterface::SCHEDULE_DAYS, message="Choose a valid day type. Allowed: {{ choices }}")
     * })
     * @Assert\Type("array")
     */
    private $days = [];

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Range(
     *      min = 0,
     *      max = 7,
     *      notInRangeMessage = "You must be between {{ min }}cm and {{ max }}cm tall to enter",
     * )
     * @Assert\Type("integer")
     */
    private $shift;

    /**
     * @var FileExportPayload
     *
     * @Serializer\Type(FileExportPayload::class)
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type(FileExportPayload::class)
     * @Assert\Valid()
     */
    private $export;

    /**
     * @var MerchantSendPayload
     *
     * @Serializer\Type(MerchantSendPayload::class)
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type(MerchantSendPayload::class)
     * @Assert\Valid()
     */
    private $send;

    /**
     * @var bool
     *
     * @Serializer\Type("bool")
     *
     * @Assert\NotNull()
     */
    private $active = false;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TransfersSchedulerPayload
     */
    public function setName(string $name): TransfersSchedulerPayload
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return iterable
     */
    public function getDays(): iterable
    {
        return $this->days;
    }

    /**
     * @param iterable $days
     * @return TransfersSchedulerPayload
     */
    public function setDays(iterable $days): TransfersSchedulerPayload
    {
        $this->days = $days;
        return $this;
    }

    /**
     * @return int
     */
    public function getShift(): int
    {
        return $this->shift;
    }

    /**
     * @param int $shift
     * @return TransfersSchedulerPayload
     */
    public function setShift(int $shift): TransfersSchedulerPayload
    {
        $this->shift = $shift;
        return $this;
    }

    /**
     * @return FileExportPayload
     */
    public function getExport(): FileExportPayload
    {
        return $this->export;
    }

    /**
     * @param FileExportPayload $export
     * @return TransfersSchedulerPayload
     */
    public function setExport(FileExportPayload $export): TransfersSchedulerPayload
    {
        $this->export = $export;
        return $this;
    }

    /**
     * @return MerchantSendPayload
     */
    public function getSend(): MerchantSendPayload
    {
        return $this->send;
    }

    /**
     * @param MerchantSendPayload $send
     * @return TransfersSchedulerPayload
     */
    public function setSend(MerchantSendPayload $send): TransfersSchedulerPayload
    {
        $this->send = $send;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return TransfersSchedulerPayload
     */
    public function setActive(bool $active): TransfersSchedulerPayload
    {
        $this->active = $active;
        return $this;
    }
}