<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;


use PayPo\Order\API\Contracts\Payloads\PayloadInterface;

class AbstractPayload implements PayloadInterface
{

}