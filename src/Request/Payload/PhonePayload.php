<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class PhonePayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\+0-9\s]{10,15}$/", message="This value is not a valid phone number.")
     */
    private $value;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value ?? '';
    }

    /**
     * @param string $value
     * @return PhonePayload
     */
    public function setValue(string $value): PhonePayload
    {
        $this->value = $value;
        return $this;
    }
}
