<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class EmailPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $value;


    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value ?? '';
    }

    /**
     * @param string $value
     * @return EmailPayload
     */
    public function setValue(string $value): EmailPayload
    {
        $this->value = $value;
        return $this;
    }
}