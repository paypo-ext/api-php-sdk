<?php

declare(strict_types=1);

namespace PayPo\Order\API\Request\Payload;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateTransactionPayload
{
    const STATUSES = [self::COMPLETED, self::CANCELED];

    const COMPLETED = 'COMPLETED';

    const CANCELED = 'CANCELED';

    /**
     * @Assert\Choice(choices=UpdateTransactionPayload::STATUSES, message="Choose a valid status.")
     *
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return UpdateTransactionPayload
     */
    public function setStatus(string $status): UpdateTransactionPayload
    {
        $this->status = $status;

        return $this;
    }
}