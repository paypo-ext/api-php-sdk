<?php

declare(strict_types=1);

namespace  PayPo\Order\API\Request\Payload;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class OAuthPayload extends AbstractPayload
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private $identifier;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     *
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private $secret;

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return self
     */
    public function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     *
     * @return self
     */
    public function setSecret(string $secret)
    {
        $this->secret = $secret;

        return $this;
    }
}
