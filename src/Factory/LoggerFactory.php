<?php

declare(strict_types=1);

namespace PayPo\Order\API\Factory;


use Monolog\Handler\FilterHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PayPo\Order\API\Contracts\Factory\LoggerFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class LoggerFactory
 * @package PayPo\Order\API\Factory
 */
class LoggerFactory implements LoggerFactoryInterface
{
    /**
     * @var string
     */
    private $cacheDir;

    /**
     * LoggerFactory constructor.
     * @param string $cacheDir
     */
    public function __construct(string $cacheDir)
    {
        $this->cacheDir = $cacheDir;
    }

    /**
     * @inheritDoc
     */
    public function create(): LoggerInterface
    {
        $logger = new Logger(self::ORDER_API_SDK_LOGGER);
        $logDir = $this->cacheDir . DIRECTORY_SEPARATOR;

        $logger->pushHandler(
            new FilterHandler(
                new StreamHandler($logDir . self::INFO_LOG_FILE, Logger::DEBUG),
                [Logger::DEBUG,Logger::INFO]
            )
        );

        $logger->pushHandler(
            new FilterHandler(
                new StreamHandler($logDir . self::NOTICE_LOG_FILE, Logger::NOTICE),
                [Logger::NOTICE, Logger::WARNING]
            )
        );

        $logger->pushHandler(
            new FilterHandler(
                new StreamHandler($logDir . self::ERROR_LOG_FILE, Logger::ERROR),
                [Logger::ERROR, Logger::CRITICAL, Logger::ALERT, Logger::EMERGENCY]
            )
        );

        return $logger;
    }
}