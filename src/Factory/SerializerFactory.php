<?php

declare(strict_types=1);

namespace PayPo\Order\API\Factory;


use PayPo\Order\API\Contracts\Factory\SerializerFactoryInterface;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

/**
 * Class SerializerFactory
 * @package PayPo\Order\API\Factory
 */
class SerializerFactory implements SerializerFactoryInterface
{

    /**
     * @var SerializerInterface
     */
    private static $serializer;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var bool
     */
    private $debug;


    /**
     * SerializerFactory constructor.
     * @param string $cacheDir
     * @param bool $debug
     */
    public function __construct(string $cacheDir, bool $debug = false)
    {
        $this->cacheDir = $cacheDir;
        $this->debug = $debug;
    }

    /**
     * @return SerializerInterface
     */
    public function create(): SerializerInterface
    {
        if (!static::$serializer) {
            $builder = SerializerBuilder::create();
            $builder
                ->setCacheDir($this->cacheDir)
                ->setDebug($this->debug)
                ->setSerializationContextFactory(
                    function () {
                        return SerializationContext::create()->setSerializeNull(true);
                    }
                )
                ->setPropertyNamingStrategy(
                    new SerializedNameAnnotationStrategy(
                        new  IdenticalPropertyNamingStrategy()
                    )
                );

            static::$serializer = $builder->build();
        }

        return static::$serializer;
    }
}
