<?php

declare(strict_types=1);

namespace PayPo\Order\API\Factory\Impl;


use PayPo\Order\API\Contracts\Factory\OACImplFactoryInterface;
use PayPo\Order\API\Contracts\Impl\GenericOACInterface;
use PayPo\Order\API\Factory\LoggerFactory;
use PayPo\Order\API\Factory\SerializerFactory;
use Symfony\Component\HttpClient\HttpClient;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractOACImplFactory implements OACImplFactoryInterface
{
    /**
     * @var GenericOACInterface
     */
    private $instance;

    /**
     * @var string
     */
    private $baseUrl;
    /**
     * @var string
     */
    private $oauthClientId;
    /**
     * @var string
     */
    private $oauthClientSecret;
    /**
     * @var string
     */
    private $oauthClientScopes;


    /**
     * @return string
     */
    abstract protected function getImplClass(): string;

    /**
     * AbstractAWCImplFactory constructor.
     * @param string $baseUrl
     * @param string $oauthClientId
     * @param string $oauthClientSecret
     * @param string $oauthClientScopes
     */
    public function __construct(
        string $baseUrl = GenericOACInterface::IMPL_URI,
        string $oauthClientId,
        string $oauthClientSecret,
        string $oauthClientScopes
    )
    {
        $this->baseUrl = $baseUrl;
        $this->oauthClientId = $oauthClientId;
        $this->oauthClientSecret = $oauthClientSecret;
        $this->oauthClientScopes = $oauthClientScopes;
    }

    /**
     * @inheritDoc
     */
    public function create(string $cacheDir, bool $debug = false): GenericOACInterface
    {
        if (!$this->instance) {
            $implClass = $this->getImplClass();
            $this->instance = new $implClass(
                HttpClient::createForBaseUri($this->baseUrl),
                $this->oauthClientId,
                $this->oauthClientSecret,
                $this->oauthClientScopes,
                (new SerializerFactory($cacheDir, $debug))->create(),
                (new LoggerFactory($cacheDir))->create()
            );
        }

        return $this->instance;
    }
}