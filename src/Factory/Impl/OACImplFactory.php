<?php

declare(strict_types=1);

namespace PayPo\Order\API\Factory\Impl;


use PayPo\Order\API\Impl\OAClient;

class OACImplFactory extends AbstractOACImplFactory
{

    /**
     * @inheritDoc
     */
    protected function getImplClass(): string
    {
        return OAClient::class;
    }
}