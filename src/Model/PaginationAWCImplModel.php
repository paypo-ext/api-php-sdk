<?php

declare(strict_types=1);

namespace PayPo\Order\API\Model;

use PayPo\Order\API\Contracts\Entity\EntityInterface;
use PayPo\Order\API\Entity\PaginationAWCImplEntity;

class PaginationAWCImplModel extends AbstractModel
{

    /**
     * @inheritDoc
     */
    protected function getEntityClass(): EntityInterface
    {
        return new PaginationAWCImplEntity();
    }
}