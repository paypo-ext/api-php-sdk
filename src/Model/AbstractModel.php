<?php

declare(strict_types=1);

namespace PayPo\Order\API\Model;


use JMS\Serializer\SerializerInterface;
use PayPo\Order\API\Contracts\Entity\EntityInterface;
use PayPo\Order\API\Contracts\Http\ResponseCodesInterface;
use PayPo\Order\API\Contracts\Models\ModelInterface;
use PayPo\Order\API\Contracts\Payloads\PayloadInterface;
use PayPo\Order\API\Exceptions\ModelException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use RuntimeException;

abstract class AbstractModel implements ModelInterface
{
    /**
     * @var EntityInterface
     */
    private $entity;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * AbstractModel constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->entity = $this->getEntityClass();
        $this->serializer = $serializer;
    }

    /**
     * @return EntityInterface
     */
    abstract protected function getEntityClass(): EntityInterface;

    /**
     * @param EntityInterface $entity
     * @return ModelInterface
     */
    public function setEntity(EntityInterface &$entity): ModelInterface
    {
        if(get_class($entity) === get_class($this->getEntityClass())){
            $this->entity = $entity;
            return $this;
        }

        throw new ModelException(ResponseCodesInterface::HTTP_FAILED_DEPENDENCY, sprintf('Provided entity `%s` must be an instance of the class `$s`', get_class($entity), get_class($this->getEntityClass())));
    }

    /**
     * @return EntityInterface
     */
    public function getEntity(): EntityInterface
    {
        return $this->entity;
    }

    /**
     * @param PayloadInterface $payload
     * @throws ModelException
     * @throws ReflectionException
     */
    public function fillFromPayload(PayloadInterface $payload)
    {
        $serializer = $this->serializer;
        try {
            $props = array_filter(array_merge($this->toArray(), $serializer->toArray($payload)), function($var){return !is_null($var);});
            $this->entity = $serializer->deserialize(json_encode($props), get_class($this->entity), 'json');
        } catch (RuntimeException $e) {
            throw new ModelException(ResponseCodesInterface::HTTP_UNPROCESSABLE_ENTITY, $e->getMessage());
        }
    }

    /**
     * @param PayloadInterface $payload
     * @throws ModelException
     * @throws ReflectionException
     */
    public function updateFromPayload(PayloadInterface $payload)
    {

        list($entity, $props) = $this->getEntityParams();

        $payloadArr = $this->serializer->toArray($payload);
        array_map(
            function ($prop) use ($entity, $payloadArr) {
                $name = $prop->getName();
                if (array_key_exists($name, $payloadArr)) {
                    $prop->setAccessible(true);
                    $prop->setValue($entity, $payloadArr[$name]);
                }
            },
            $props
        );
    }

    /**
     * @return array
     * @throws ModelException
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        list($entity, $props) = $this->getEntityParams();
        $propsNames = array_column($props, 'name');

        $propsValues = array_map(
            function ($prop) use ($entity) {
                $prop->setAccessible(true);
                return $prop->isInitialized($entity) ? $prop->getValue($entity) : null;
            },
            $props
        );

        return array_combine($propsNames, $propsValues);
    }

    /**
     * @return array
     * @throws ModelException
     * @throws ReflectionException
     */
    public function getEntityParams(): array
    {
        if (empty($this->entity)) throw new ModelException(ResponseCodesInterface::HTTP_FAILED_DEPENDENCY, 'Entity object is empty');

        $entity = $this->entity;
        $reflect = new ReflectionClass($entity);
        $props = $reflect->getProperties(ReflectionProperty::IS_PRIVATE);
        return array($entity, $props);
    }

}