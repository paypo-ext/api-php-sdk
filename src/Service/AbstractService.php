<?php

declare(strict_types=1);

namespace PayPo\Order\API\Service;

use JMS\Serializer\SerializerInterface;
use PayPo\Order\API\Contracts\ValueObjects\ValueObjectInterface;
use PayPo\Order\API\Impl\AbstractOACImpl;

abstract class AbstractService
{
    /**
     * @var AbstractOACImpl
     */
    protected $httpClient;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * MerchantService constructor.
     * @param AbstractOACImpl $httpClient
     * @param SerializerInterface $serializer
     */
    public function __construct(AbstractOACImpl $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * @return string
     */
    abstract protected function getResponseFormat():string;

    /**
     * @param string $response
     * @param ValueObjectInterface $responseObject
     * @return ValueObjectInterface | ValueObjectCollectionInterface
     */
    protected function getResponseObject(string $response, ValueObjectInterface $responseObject): ValueObjectInterface
    {
        return $this->serializer->deserialize(
            $response,
            get_class($responseObject),
            $this->getResponseFormat()
        );
    }
}