<?php

declare(strict_types=1);

namespace PayPo\Order\API\Service;


use JMS\Serializer\SerializerInterface;
use PayPo\Order\API\Contracts\Impl\GenericOACInterface;
use PayPo\Order\API\Contracts\Impl\MerchantsOACInterface;
use PayPo\Order\API\Exceptions\OACException;
use PayPo\Order\API\Request\Payload\Merchants\MerchantPayload;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MerchantService extends AbstractService
{
    private const RESPONSE_FORMAT = 'json';

    /**
     * MerchantService constructor.
     * @param GenericOACInterface $httpClient
     * @param SerializerInterface $serializer
     */
    public function __construct(
        GenericOACInterface $httpClient,
        SerializerInterface $serializer
    )
    {
        parent::__construct($httpClient, $serializer);
    }


    /**
     * @return string
     */
    protected function getResponseFormat(): string
    {
        return self::RESPONSE_FORMAT;
    }

    /**
     * @param MerchantPayload $payload
     * @return array
     * @throws OACException
     */
    public function add(MerchantPayload $payload): array
    {
        try {
            /** @var ResponseInterface $response */
            $response = $this->httpClient->post(
                MerchantsOACInterface::HTTP_REQUEST_ENDPOINT,
                $payload
            );

            $data = $response->getContent();

            return json_decode($data, true) ?? [];
        } catch (ClientExceptionInterface | Exception | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            throw new OACException($e->getCode(), $e->getResponse()->getContent(false));
        }
    }
}