<?php

declare(strict_types=1);

namespace PayPo\Order\API\Service;

use JMS\Serializer\SerializerInterface;
use PayPo\Order\API\ClientInterface;
use PayPo\Order\API\Request\Payload\RefundsPayload;
use PayPo\Order\API\Request\Payload\RegisterTransactionPayload;
use PayPo\Order\API\Request\Payload\UpdateTransactionPayload;
use PayPo\Order\API\Response\ValueObject\MerchantTransactionValueObject;
use PayPo\Order\API\Response\ValueObject\RegisterTransactionValueObject;
use PayPo\Order\API\Response\ValueObject\StatusValueObject;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use function json_decode;

class TransactionService
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(SerializerInterface $serializer, ClientInterface $client)
    {
        $this->serializer = $serializer;
        $this->client     = $client;
    }

    /**
     * @param RegisterTransactionPayload $payload
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     *
     * @return RegisterTransactionValueObject
     */
    public function registerTransaction(RegisterTransactionPayload $payload): RegisterTransactionValueObject
    {
        return $this->processInternal(
            'POST',
            'transactions',
            RegisterTransactionValueObject::class,
            $payload
        );
    }

    /**
     * @param string         $transactionId
     * @param RefundsPayload $payload
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return StatusValueObject
     */
    public function refundsTransaction(string $transactionId, RefundsPayload $payload): StatusValueObject
    {
        return $this->processInternal(
            'POST',
            'transactions/%s/refunds',
            StatusValueObject::class,
            $payload,
            $transactionId
        );
    }

    /**
     * @param string                   $transactionId
     * @param UpdateTransactionPayload $payload
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return StatusValueObject
     */
    public function updateTransaction(string $transactionId, UpdateTransactionPayload $payload): StatusValueObject
    {
        return $this->processInternal(
            'PATCH',
            'transactions/%s',
            StatusValueObject::class,
            $payload,
            $transactionId
        );
    }

    /**
     * @param string $transactionId
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return MerchantTransactionValueObject
     */
    public function retrieveTransaction(string $transactionId): MerchantTransactionValueObject
    {
        return $this->processInternal(
            'GET',
            'transactions/%s',
            MerchantTransactionValueObject::class,
            null,
            $transactionId
        );
    }

    /**
     * @param string      $method
     * @param string      $path
     * @param string      $class
     * @param object|null $payload
     * @param string|null $transactionId
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return mixed
     */
    private function processInternal(string $method, string $path, string $class, ?object $payload, ?string $transactionId = null)
    {
        if (is_null($payload)) {
            $response = $this->client->request(
                $method,
                is_null($transactionId) ? $path : sprintf($path, $transactionId)
            );
        }
        else {
            $data = json_decode($this->serializer->serialize($payload, 'json'), true);

            $response = $this->client->request(
                $method,
                is_null($transactionId) ? $path : sprintf($path, $transactionId),
                [
                    'json' => $data,
                ]
            );
        }

        return $this->serializer->deserialize(
            $response,
            $class,
            'json'
        );
    }
}