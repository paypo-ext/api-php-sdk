<?php

declare(strict_types=1);

namespace PayPo\Order\API\Exceptions;


use Exception;

class PayloadException extends Exception
{
    /**
     * ValueObjectException constructor.
     * @param int $code
     * @param $message
     */
    public function __construct(int $code, $message)
    {
        parent::__construct($message, $code);
    }
}