<?php

declare(strict_types=1);

namespace PayPo\Order\API\Impl;


use JMS\Serializer\SerializerInterface;
use PayPo\Order\API\Contracts\Impl\GenericOACInterface;
use PayPo\Order\API\Contracts\Payloads\PayloadInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use function array_merge_recursive;

abstract class AbstractOACImpl implements GenericOACInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var array
     */
    protected $defaultOptions = [];

    /**
     * @var string
     */
    private $oauthClientId;

    /**
     * @var string
     */
    private $oauthClientSecret;

    /**
     * @var string
     */
    private $oauthClientScopes;


    /**
     * AsyncWorkerClient constructor.
     *
     * @param HttpClientInterface $httpClient
     * @param string $oauthClientId
     * @param string $oauthClientSecret
     * @param string $oauthClientScopes
     * @param SerializerInterface $serializer
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        HttpClientInterface $httpClient,
        string $oauthClientId,
        string $oauthClientSecret,
        string $oauthClientScopes,
        SerializerInterface $serializer,
        LoggerInterface $logger
    )
    {
        $this->httpClient = $httpClient;
        $this->oauthClientId = $oauthClientId;
        $this->oauthClientSecret = $oauthClientSecret;
        $this->oauthClientScopes = $oauthClientScopes;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     */
    private function authorize()
    {
        if (!isset($this->defaultOptions['auth_bearer'])) {
            $response = $this->httpClient->request(
                GenericOACInterface::HTTP_POST,
                self::IMPL_TOKEN_ENDPOINT,
                [
                    'auth_basic' => [$this->oauthClientId, $this->oauthClientSecret],
                    'body' => [
                        'grant_type' => self::IMPL_GRANT_TYPE,
                        'scopes' => $this->oauthClientScopes,
                    ]
                ]
            );

            $data = $response->toArray();

            if (false === isset($data['access_token'])) {
                throw new ClientException($response);
            }

            $this->defaultOptions = [
                'auth_bearer' => $data['access_token']
            ];
        }
    }

    /**
     * @param $url
     * @param PayloadInterface $payload
     * @param array $options
     * @return ResponseInterface
     */
    public function post($url, PayloadInterface $payload, array $options = []): ResponseInterface
    {
        $options['json'] = $this->serializer->toArray($payload);

        return $this->request(
            GenericOACInterface::HTTP_POST,
            $url,
            $options
        );
    }

    /**
     * @param $url
     * @param PayloadInterface $payload
     * @param array $options
     * @return ResponseInterface
     */
    public function patch($url, PayloadInterface $payload, array $options = []): ResponseInterface
    {
        $options['json'] = $this->serializer->toArray($payload);

        return $this->request(
            GenericOACInterface::HTTP_PATCH,
            $url,
            $options
        );
    }

    /**
     * @param $url
     * @param PaginationInterface $pagination
     * @param FilterInterface|null $filter
     * @param array $options
     * @return ResponseInterface
     */
    public function get($url, PaginationInterface $pagination, FilterInterface $filter = null, array $options = []): ResponseInterface
    {
        $options['query']['pagination'] = $this->serializer->toArray($pagination);

        if ($filter) {
            $options['query']['filter'] = $this->serializer->toArray($filter);
        }

        return $this->request(
            GenericOACInterface::HTTP_GET,
            $url,
            $options
        );
    }

    /**
     * @param $url
     * @param array $options
     * @return ResponseInterface
     */
    public function delete($url, array $options = []): ResponseInterface
    {
        return $this->request(
            GenericOACInterface::HTTP_GET,
            $url,
            $options
        );
    }

    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return mixed
     */
    public function request($method, $url, array $options = []): ResponseInterface
    {
        if (!isset($this->defaultOptions['auth_bearer'])) {
            $this->authorize();
        }

        $options = array_merge_recursive($options, $this->defaultOptions);

        return $this->httpClient->request($method, $url, $options);
    }
}