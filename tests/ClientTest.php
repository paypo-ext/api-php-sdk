<?php

declare(strict_types=1);

namespace PayPo\Order\API\Tests;

use PayPo\Order\API\Client;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class ClientTest extends TestCase
{
    public function testRequest()
    {
        $transactionBody = '{"code":200,"message":"Transaction updated successfully"}';

        $httpClient = TestUtils::createHttpClient($transactionBody, 200);
        $client     = new Client($httpClient, 'login', 'password');

        $response = $client->request(
            'PATCH',
            'transactions/4ad35d14-ed89-42d1-9796-4f48449d287f',
            [
                'json' => '{"status": "COMPLETED"}',
            ]
        );

        self::assertEquals($transactionBody, $response);
    }

    public function testRequest_withAuthError()
    {
        $transactionBody = '{"code":200,"message":"Transaction updated successfully"}';

        $responses = [
            TestUtils::createTokenResponse(),
            new MockResponse(
                '',
                [
                    'http_code' => 401,
                ]
            ),
            TestUtils::createTokenResponse(),
            new MockResponse(
                $transactionBody,
                [
                    'response_headers' => [
                        'Content-Length' => strlen($transactionBody),
                        'Content-Type'   => 'application/json',
                    ],
                    'http_code'        => 200,
                ]
            ),
        ];

        $httpClient = new MockHttpClient($responses, TestUtils::BASE_URI);
        $client     = new Client($httpClient, 'login', 'password');

        $response = $client->request(
            'PATCH',
            'transactions/4ad35d14-ed89-42d1-9796-4f48449d287f',
            [
                'json' => '{"status": "COMPLETED"}',
            ]
        );

        self::assertEquals($transactionBody, $response);
    }

    public function testRetrieveAccessToken()
    {
        $httpClient = TestUtils::createHttpClientTokenOnly();
        $client     = new Client($httpClient);

        $token = $client->retrieveAccessToken('login', 'password');
        self::assertEquals('eyJ0eXAiOiJKV', $token);
    }
}