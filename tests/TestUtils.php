<?php

namespace PayPo\Order\API\Tests;

use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TestUtils
{
    const TOKEN_BODY = '{"token_type":"Bearer","expires_in":1800,"access_token":"eyJ0eXAiOiJKV"}';

    const BASE_URI = 'http://api.paypo.local/v3/';

    public static function createHttpClient(string $body, int $code): HttpClientInterface
    {
        $responses = [
            self::createTokenResponse(),
            new MockResponse(
                $body,
                [
                    'response_headers' => [
                        'Content-Length' => strlen($body),
                        'Content-Type'   => 'application/json',
                    ],
                    'http_code'        => $code,
                ]
            ),
        ];

        return new MockHttpClient($responses, self::BASE_URI);
    }

    public static function createHttpClientTokenOnly(): HttpClientInterface
    {
        $responses = [
            self::createTokenResponse(),
        ];

        return new MockHttpClient($responses, self::BASE_URI);
    }

    public static function createTokenResponse(): MockResponse
    {
        return new MockResponse(
            self::TOKEN_BODY,
            [
                'response_headers' => [
                    'Content-Length' => strlen(self::TOKEN_BODY),
                    'Content-Type'   => 'application/json',
                ],
            ]
        );
    }
}