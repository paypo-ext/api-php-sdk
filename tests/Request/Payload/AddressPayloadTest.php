<?php

declare(strict_types=1);

namespace PayPo\Order\API\Tests\Request\Payload;

use PayPo\Order\API\Request\Payload\AddressPayload;
use PHPUnit\Framework\TestCase;

class AddressPayloadTest extends TestCase
{
    public function testNormalizeAddressPayload()
    {
        $payload = new AddressPayload();
        $payload->setCity('ąćżźśó');
        $payload->setStreet('ąćżźśó');
        self::assertEquals('ĄĆŻŹŚÓ', $payload->getCity());
        self::assertEquals('ĄĆŻŹŚÓ', $payload->getStreet());
    }
}