<?php

declare(strict_types=1);

namespace PayPo\Order\API\Tests\Request\Payload;

use PayPo\Order\API\Request\Payload\CustomerPayload;
use PHPUnit\Framework\TestCase;

class CustomerPayloadTest extends TestCase
{
    public function testCustomerPayload()
    {
        $payload = new CustomerPayload();
        $payload->setName('ąćżźśó');
        $payload->setSurname('ąćżźśó');
        self::assertEquals('ĄĆŻŹŚÓ', $payload->getName());
        self::assertEquals('ĄĆŻŹŚÓ', $payload->getSurname());
    }
}