<?php

declare(strict_types=1);

namespace PayPo\Order\API\Tests\Service;

use DateTimeInterface;
use PayPo\Order\API\Client;
use PayPo\Order\API\Factory\SerializerFactory;
use PayPo\Order\API\Request\Payload\AddressPayload;
use PayPo\Order\API\Request\Payload\ConfigurationPayload;
use PayPo\Order\API\Request\Payload\CustomerPayload;
use PayPo\Order\API\Request\Payload\OrderPayload;
use PayPo\Order\API\Request\Payload\RefundsPayload;
use PayPo\Order\API\Request\Payload\RegisterTransactionPayload;
use PayPo\Order\API\Request\Payload\UpdateTransactionPayload;
use PayPo\Order\API\Service\TransactionService;
use PayPo\Order\API\Tests\TestUtils;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TransactionServiceTest extends TestCase
{
    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testRegisterTransaction()
    {
        $transactionBody = '{"transactionId":"4ad35d14-ed89-42d1-9796-4f48449d287f","redirectUrl":"https://process.paypo.local/4ad35d14-ed89-42d1-9796-4f48449d287f"}';

        $client = TestUtils::createHttpClient($transactionBody, 201);

        $configuration = new ConfigurationPayload();
        $configuration->setNotifyUrl('https://merchant.com/notify');
        $configuration->setReturnUrl('https://merchant.com/return');

        $customer = new CustomerPayload();
        $customer->setName('Janusz');
        $customer->setSurname('Testowy');
        $customer->setEmail('janusz.testowy@paypo.pl');
        $customer->setPhone('000000000');

        $order = new OrderPayload();
        $order->setAmount(1000);
        $order->setReferenceId('de2a4b55-39ed-4d8f-9a81-0d932907182e');
        $order->setBillingAddress($this->createAddress());
        $order->setShippingAddress($this->createAddress());

        $register = new RegisterTransactionPayload();
        $register->setConfiguration($configuration);
        $register->setCustomer($customer);
        $register->setOrder($order);

        $response = $this->createService($client)->registerTransaction($register);

        self::assertEquals('4ad35d14-ed89-42d1-9796-4f48449d287f', $response->getTransactionId());
        self::assertEquals(
            'https://process.paypo.local/4ad35d14-ed89-42d1-9796-4f48449d287f',
            $response->getRedirectUrl()
        );
    }

    /**
     * @depends testRegisterTransaction
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateTransaction_setCompleted()
    {
        $transactionBody = '{"code":200,"message":"Transaction updated successfully"}';

        $client = TestUtils::createHttpClient($transactionBody, 200);

        $update = new UpdateTransactionPayload();
        $update->setStatus('COMPLETED');

        $response = $this->createService($client)->updateTransaction('4ad35d14-ed89-42d1-9796-4f48449d287f', $update);
        self::assertEquals(200, $response->getCode());
        self::assertEquals('Transaction updated successfully', $response->getMessage());
    }

    /**
     * @depends testRegisterTransaction
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateTransaction_setCanceled()
    {
        $transactionBody = '{"code":200,"message":"Transaction updated successfully"}';

        $client = TestUtils::createHttpClient($transactionBody, 200);

        $update = new UpdateTransactionPayload();
        $update->setStatus('CANCELED');

        $response = $this->createService($client)->updateTransaction('4ad35d14-ed89-42d1-9796-4f48449d287f', $update);
        self::assertEquals(200, $response->getCode());
        self::assertEquals('Transaction updated successfully', $response->getMessage());
    }

    /**
     * @depends testRegisterTransaction
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testRefundTransaction()
    {
        $transactionBody = '{"code":201,"message":"Refund created successfully"}';

        $client = TestUtils::createHttpClient($transactionBody, 201);

        $refund = new RefundsPayload();
        $refund->setAmount(1000);

        $response = $this->createService($client)->refundsTransaction('4ad35d14-ed89-42d1-9796-4f48449d287f', $refund);
        self::assertEquals(201, $response->getCode());
        self::assertEquals('Refund created successfully', $response->getMessage());
    }

    /**
     * @depends testRegisterTransaction
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testRetrieveTransaction()
    {
        $transactionBody = '{"merchantId":"c15acf35-db3a-4d19-9daf-9a91450ce1a6","referenceId":"de2a4b55-39ed-4d8f-9a81-0d932907182e","transactionId":"1b7af1e8-93fe-4e3b-a781-95e7806a5795","transactionStatus":"COMPLETED","amount":1000,"settlementStatus":"CONFIRMED","lastUpdate":"2021-01-19T14:03:05+00:00"}';

        $client = TestUtils::createHttpClient($transactionBody, 200);

        $refund = new RefundsPayload();
        $refund->setAmount(1000);

        $response = $this->createService($client)->retrieveTransaction('4ad35d14-ed89-42d1-9796-4f48449d287f');
        self::assertEquals('c15acf35-db3a-4d19-9daf-9a91450ce1a6', $response->getMerchantId());
        self::assertEquals('de2a4b55-39ed-4d8f-9a81-0d932907182e', $response->getReferenceId());
        self::assertEquals('1b7af1e8-93fe-4e3b-a781-95e7806a5795', $response->getTransactionId());
        self::assertEquals('COMPLETED', $response->getTransactionStatus());
        self::assertEquals(1000, $response->getAmount());
        self::assertEquals('CONFIRMED', $response->getSettlementStatus());
        self::assertEquals(
            '2021-01-19T14:03:05.000+00:00',
            $response->getLastUpdate()->format(DateTimeInterface::RFC3339_EXTENDED)
        );
    }

    private function createService(HttpClientInterface $client): TransactionService
    {
        return new TransactionService(
            (new SerializerFactory('/tmp/order-api-sdk', false))->create(),
            new Client($client, 'login', 'password')
        );
    }

    private function createAddress(): AddressPayload
    {
        $address = new AddressPayload();
        $address->setCity('Testowo');
        $address->setStreet('Testowa');
        $address->setBuilding('10');
        $address->setFlat('10');
        $address->setZip('00-000');

        return $address;
    }
}
