<?php

declare(strict_types=1);

require __DIR__.'/../vendor/autoload.php';

use PayPo\Order\API\ClientFactory;
use PayPo\Order\API\Factory\SerializerFactory;
use PayPo\Order\API\Request\Payload\AddressPayload;
use PayPo\Order\API\Request\Payload\ConfigurationPayload;
use PayPo\Order\API\Request\Payload\CustomerPayload;
use PayPo\Order\API\Request\Payload\OrderPayload;
use PayPo\Order\API\Request\Payload\RegisterTransactionPayload;
use PayPo\Order\API\Service\TransactionService;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;

// parameters
$parameters = [
    'debug'         => false, // default: false
    'cache_dir'     => __DIR__.'/var/cache',
    'uri'           => 'https://api.sandbox.paypo.pl/v3/',
    'client_id'     => '',
    'client_secret' => '',
    'merchant'      => ''
];

// services
$serializer         = (new SerializerFactory($parameters['cache_dir'], $parameters['debug']))->create();
$client             = (new ClientFactory($parameters['uri'], $parameters['client_id'], $parameters['client_secret']))->create();
$transactionService = new TransactionService($serializer, $client);

// app
$payload = new RegisterTransactionPayload();
$payload
    ->setMerchantId($parameters['merchant'])
    ->setCustomer(
        (new CustomerPayload())
            ->setName('Tadeusz')
            ->setSurname('Drozda')
            ->setEmail('tadeusz.drozda@paypo.pl')
            ->setPhone('223337460')
    )
    ->setOrder(
        (new OrderPayload())
            ->setReferenceId('b0762d0c-7cfa-449f-8ab8-fca9185afb0b')
            ->setAmount(9999)
            ->setBillingAddress(
                (new AddressPayload())
                    ->setCountry('PL')
                    ->setZip('02-672')
                    ->setCity('Warszawa')
                    ->setStreet('Domaniewska')
                    ->setBuilding('37')
            )
            ->setShippingAddress(
                (new AddressPayload())
                    ->setCountry('PL')
                    ->setZip('02-672')
                    ->setCity('Warszawa')
                    ->setStreet('Domaniewska')
                    ->setBuilding('37')
            )
    )
    ->setConfiguration(
        (new ConfigurationPayload())
            ->setReturnUrl('https://shop.sandbox.paypo.pl/return')
            ->setNotifyUrl('https://shop.sandbox.paypo.pl/notify')
            ->setCancelUrl('https://shop.sandbox.paypo.pl/cancel')
    )
;

try {
    $result = $transactionService->registerTransaction($payload);
} catch (\Throwable $e) {
    if ($e instanceof HttpExceptionInterface) {
        $result = \sprintf('%s | %s', $e->getResponse()->getContent(false), $e->getMessage());
    }
    else{
        $result = $e->getMessage();
    }
}

var_dump($result);
